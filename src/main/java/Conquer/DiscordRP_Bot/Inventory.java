/**
 * File: Inventory.java
 * Author: Matt Wong, Kyle Fleskes
 * Editors: Matt Wong, Kyle Fleskes
 * Desc: Represents a player character's inventory.
 */
package Conquer.DiscordRP_Bot;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Set;

public class Inventory {
	
	private HashMap<Item, Integer> money;
	private HashMap<Item, Integer> equipment;
	private HashMap<Item, Integer> consumable;
	private HashMap<Item, Integer> important;
	private HashMap<Item, Integer> misc;
	
	private String characterName;
	private String fileName;
	
	/**
	 * Constructor requires to take in a character name
	 * @param name
	 */
	public Inventory(String name) {
		money = new HashMap<Item, Integer>();
		equipment = new HashMap<Item, Integer>();
		consumable = new HashMap<Item, Integer>();
		important = new HashMap<Item, Integer>();
		misc = new HashMap<Item, Integer>();
		
		characterName = name;
		fileName = name + "_inven.txt";
		File file = new File(BotInfo.DBpath + BotInfo.DB_inventories + "/" + fileName);
		
		if(file.exists()) 
			read();
		else 
			save();
	}
	
	/**
	 * Adds or removes a number of items to the inventory
	 * @param toAdd
	 */
	public void add(Item toAdd, int amount) {
		String type = toAdd.getType();
		
		//finds type and add by amount
		switch(type) {
		
		case "currency":
			if(money.containsKey(toAdd))
				money.put(toAdd, money.get(toAdd) + amount);
			else
				money.put(toAdd, amount);
			
			//if item count is 0, remove item
			if(money.get(toAdd) <= 0)
				money.remove(toAdd);
			break;
		
		case "equipment":
			if(equipment.containsKey(toAdd))
				equipment.put(toAdd, equipment.get(toAdd) + amount);
			else
				equipment.put(toAdd, amount);
			
			//if item count is 0, remove item
			if(equipment.get(toAdd) <= 0)
				equipment.remove(toAdd);
			break;
			
		case "consumable":
			if(consumable.containsKey(toAdd))
				consumable.put(toAdd, consumable.get(toAdd) + amount);
			else
				consumable.put(toAdd, amount);
			
			//if item count is 0, remove item
			if(consumable.get(toAdd) <= 0)
				consumable.remove(toAdd);
			break;
			
		case "important":
			if(important.containsKey(toAdd))
				important.put(toAdd, important.get(toAdd) + amount);
			else
				important.put(toAdd, amount);
			
			//if item count is 0, remove item
			if(important.get(toAdd) <= 0)
				important.remove(toAdd);
			break;
			
		case "misc":
			if(misc.containsKey(toAdd))
				misc.put(toAdd, misc.get(toAdd) + amount);
			else
				misc.put(toAdd, amount);
			
			//if item count is 0, remove item
			if(misc.get(toAdd) <= 0)
				misc.remove(toAdd);
			break;
		}
		save();
	}
	
	/**
	 * Searches and removes the number of items with name passed in
	 * @param itemName
	 * @return
	 */
	public void remove(Item itemName, int amount) {
		
		if(money.containsKey(itemName)) {
			if(money.get(itemName) - amount > 1) 
				money.put(itemName, money.get(itemName) - amount);
			else
				money.remove(itemName);
		}
		else if(equipment.containsKey(itemName)) {
			if(equipment.get(itemName) - amount> 1) 
				equipment.put(itemName, equipment.get(itemName) - amount);
			else
				equipment.remove(itemName);
		}
		else if(consumable.containsKey(itemName)) {
			if(consumable.get(itemName) - amount > 1) 
				consumable.put(itemName, consumable.get(itemName) - amount);
			else
				consumable.remove(itemName);
		}
		else if(important.containsKey(itemName)) {
			if(important.get(itemName) - amount > 1) 
				important.put(itemName, important.get(itemName) - amount);
			else
				important.remove(itemName);
		}
		else if(misc.containsKey(itemName)) {
			if(misc.get(itemName) - amount > 1) 
				misc.put(itemName, misc.get(itemName) - amount);
			else
				misc.remove(itemName);
		}
		save();
	}
	
	/**
	 * Finds and gets the amount of passed in 
	 * @param item
	 * @return
	 */
	public int get(Item item) {
		if(money.containsKey(item))
			return money.get(item);
		else if(equipment.containsKey(item))
			return equipment.get(item);
		else if(consumable.containsKey(item))
			return consumable.get(item);
		else if(important.containsKey(item))
			return important.get(item);
		else if(misc.containsKey(item))
			return misc.get(item);
		else
			return 0;
	}
	
	/**
	 * Performs read() again
	 */
	public void refresh() {
		money.clear();
		equipment.clear();
		consumable.clear();
		important.clear();
		misc.clear();
		read();
	}
	
	
	/**
	 * Checks if passed in item is in inventory
	 * @param item
	 * @return
	 */
	public boolean contains(Item item) {
		return money.containsKey(item) || equipment.containsKey(item) || consumable.containsKey(item) || important.containsKey(item) || misc.containsKey(item);
	}
	
	/**
	 * Returns the count of unique items
	 * @return
	 */
	public int countUniqueItems() {
		return money.size() + equipment.size() + consumable.size() + important.size() + misc.size();
	}
	
	/**
	 * Returns specified category
	 * @param cat
	 * @return
	 */
	public HashMap<Item, Integer> getCategory(String cat) {
		if(cat.equals("currency"))
			return money;
		else if(cat.equals("equipment"))
			return equipment;
		else if(cat.equals("consumable"))
			return consumable;
		else if(cat.equals("important"))
			return important;
		else 
			return misc;
	}
	
	/**
	 * Takes in a string and search through entire inventory and finds all items with string
	 * @param str
	 * @return
	 */
	public HashMap<Item, Integer> search(String str) {
		HashMap<Item, Integer> toRet = new HashMap<Item, Integer>();
		
		for(Item i : money.keySet()) {
			if(i.name.contains(str) || i.description.contains(str))
				toRet.put(i, money.get(i));
		}
		
		for(Item i : equipment.keySet()) {
			if(i.name.contains(str) || i.description.contains(str))
				toRet.put(i, equipment.get(i));
		}
		
		for(Item i : consumable.keySet()) {
			if(i.name.contains(str) || i.description.contains(str))
				toRet.put(i, consumable.get(i));
		}
		
		for(Item i : important.keySet()) {
			if(i.name.contains(str) || i.description.contains(str))
				toRet.put(i, important.get(i));
		}
		
		for(Item i : misc.keySet()) {
			if(i.name.contains(str) || i.description.contains(str))
				toRet.put(i, misc.get(i));
		}
		
		return toRet;
	}
	
	/**
	 * Clears the entire inventory
	 */
	public void clearInventory() {
		money.clear();
		equipment.clear();
		consumable.clear();
		important.clear();
		misc.clear();
		save();
	}
	
	/**
	 * Returns a all items of certain category in a String
	 * @param type
	 * @return
	 */
	public String print(String type) {
		HashMap<Item, Integer> toPrint = null;
		switch(type) {
		
		case "money":
			toPrint = money;
			break;
		case "currency":
			toPrint = money;
			break;
		case "equipment":
			toPrint = equipment;
			break;
		case "consumable":
			toPrint = consumable;
			break;
		case "important":
			toPrint = important;
			break;
		case "misc":
			toPrint = misc;
			break;
		default:
			toPrint = null;
			break;
		}
		
		if(toPrint == null)
			return null;
		
		String toRet = "**" + type + "**" + ":\n";
		int cnt = 1;
		for(Item i : toPrint.keySet()) {
			toRet += cnt + ". " + i.getName() + " - " + toPrint.get(i) + "\n";
			cnt++;
		}
		return toRet;
	}
	
	/**
	 * Returns the entire inventory in a string with the following order
	 * money
	 * equipment
	 * consumable
	 * important
	 * misc
	 */
	public String toString() {
		return print("currency")
				+ print("equipment")
				+ print("consumable")
				+ print("important")
				+ print("misc");
	}
	
	/**
	 * Reads and updates link lists according to data from database
	 */
	private void read() {
		File file = new File(BotInfo.DBpath + BotInfo.DB_inventories + "/" + fileName);
		
		try {
			
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String arg1 = reader.readLine(); 
			if(arg1 == null || arg1.equals(""))
				return;
			
			characterName = arg1;
			arg1 = reader.readLine();
			String arg2 = reader.readLine();
			
			//scans file and fills inventory
			while (arg1 != null && arg2 != null && !arg1.equals("") && !arg2.equals("")) {
				if(App.items.containsKey(arg1)) {
					Item toAdd = App.items.get(arg1);
					String type = toAdd.getType();
					
					int amount = Integer.parseInt(arg2);
					
					if(type.equals("currency"))
						money.put(toAdd, amount);
					else if(type.equals("equipment"))
						equipment.put(toAdd, amount);
					else if(type.equals("consumable"))
						consumable.put(toAdd, amount);
					else if(type.equals("important"))
						important.put(toAdd, amount);
					else 
						misc.put(toAdd, amount);
				}
				
				arg1 = reader.readLine();
				arg2 = reader.readLine();
			}
			reader.close();
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * Writes all information from the link lists to the database
	 */
	private void save() {
		
		File file = new File(BotInfo.DBpath + BotInfo.DB_inventories + "/" + fileName);
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			//first thing to write is the character name
			writer.write(characterName);
			writer.newLine();
			
			/*
			 * Writes in this order for each item:
			 * <name of item>
			 * <amount>
			 */
			//writes money first
			for(Item i : money.keySet()) {
				writer.write(i.getName());
				writer.newLine();
				writer.write(money.get(i) + "");
				writer.newLine();
			}
			
			//writes equipments
			for(Item i : equipment.keySet()) {
				writer.write(i.getName());
				writer.newLine();
				writer.write(equipment.get(i) + "");
				writer.newLine();
			}
			
			//writes consumables
			for(Item i : consumable.keySet()) {
				writer.write(i.getName());
				writer.newLine();
				writer.write(consumable.get(i) + "");
				writer.newLine();
			}
			
			//writes importants
			for(Item i : important.keySet()) {
				writer.write(i.getName());
				writer.newLine();
				writer.write(important.get(i) + "");
				writer.newLine();
			}
			
			//write miscs
			for(Item i : misc.keySet()) {
				writer.write(i.getName());
				writer.newLine();
				writer.write(misc.get(i) + "");
				writer.newLine();
			}
			
			writer.close();
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
	}
}