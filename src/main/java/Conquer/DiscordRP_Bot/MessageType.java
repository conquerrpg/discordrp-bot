package Conquer.DiscordRP_Bot;

import java.awt.Color;

import net.dv8tion.jda.core.EmbedBuilder;

public class MessageType {

	public static EmbedBuilder SYSTEM() {
		EmbedBuilder eb = new EmbedBuilder();
		
		eb.setTitle("System");
		eb.setColor(BotInfo.systemColor);
		
		return eb;
	}
	
	public static EmbedBuilder DB() {
		EmbedBuilder eb = new EmbedBuilder();
		
		eb.setTitle("Database");
		eb.setColor(BotInfo.dbColor);
		
		return eb;
		
	}
	
	public static EmbedBuilder ERROR() {
		EmbedBuilder eb = new EmbedBuilder();
		
		eb.setTitle("Error");
		eb.setColor(BotInfo.errColor);
		
		return eb;
	}
	
	public static EmbedBuilder WARNING() {
		EmbedBuilder eb = new EmbedBuilder();
		
		eb.setTitle("Warning");
		eb.setColor(BotInfo.warningColor);
		
		return eb;
	}
	
	public static EmbedBuilder GENERAL() {
		EmbedBuilder eb = new EmbedBuilder();
		
		eb.setTitle("Notice");
		eb.setColor(BotInfo.generalColor);
		
		return eb;
	}
	
	public static EmbedBuilder INVENTORY() {
		EmbedBuilder eb = new EmbedBuilder();
		
		eb.setTitle("Inventory");
		eb.setColor(BotInfo.inventoryColor);
		
		return eb;
	}
	
	public static EmbedBuilder SPELLBOOK() {
		EmbedBuilder eb = new EmbedBuilder();
		
		eb.setTitle("Spellbook");
		eb.setColor(BotInfo.spellbookColor);
		
		return eb;
	}
	
	public static EmbedBuilder MP() {
		EmbedBuilder eb = new EmbedBuilder();
		
		eb.setTitle("MP");
		eb.setColor(BotInfo.mpColor);
		
		return eb;
	}
}
