package Conquer.DiscordRP_Bot;

public interface Entity {
	
	/**
	 * This was solely made for the 'search' command where an Entity would return all its values in a long
	 * string
	 * @return
	 */
	public String allStrings();
}
