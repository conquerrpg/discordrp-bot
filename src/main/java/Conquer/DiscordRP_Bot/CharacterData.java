/**
 * File: CharacterData.java
 * Author: Matt Wong
 * Editors: Matt Wong
 * Desc: Represents a character in the world. Class for NPCs, superclass for hero
 * characters
 */
package Conquer.DiscordRP_Bot;
import java.io.*;
import java.util.*;

public class CharacterData implements Entity {

	private String firstName;
	private String fullName;
	private String nickname;
	private String gender;
	
	private String race;
	private String age;
	
	private int mp;
	private int mpCap; 
	private Inventory backpack;
	private Spellbook spellbook;
	
	/**
	 * Constructor 
	 * @param fullName
	 * @param nickname
	 * @param race
	 * @param gender
	 * @param age
	 */
	public CharacterData(String fullName, String nickname, String gender, String race, String age) {
		this.fullName = fullName;
		firstName = fullName.split(" ")[0];
		this.nickname = nickname;
		this.race = race;
		this.gender = gender;
		this.age = age;
		this.mpCap = 100;
		this.mp = 100;
		
		String fname = fullName.replaceAll(" ", "_").trim();
		
		backpack = new Inventory(fname);
		spellbook = new Spellbook(fname);
		
		save();
		
	}
	
	/**
	 * Constructor with full name of character
	 * @param name
	 */
	public CharacterData(String fullName) {
		
		this.fullName = fullName;
		
		String fileName = fullName.replaceAll(" ", "_").trim() + "_cd.txt";
		File file = new File(BotInfo.DBpath + BotInfo.DB_characters + "/" + fileName);
		if(file.exists())
			load(file);
		else {
			this.fullName = fullName;
			firstName = fullName.split(" ")[0];
			this.nickname = "null";
			this.race = "null";
			this.gender = "null";
			this.age = "null";
			this.mpCap = 100;
			this.mp = 100;
		}
		
		String fname = fullName.replaceAll(" ", "_").trim();
		backpack = new Inventory(fname);
		spellbook = new Spellbook(fname);
		save();
		
	}
	
	/**
	 * Constructor with just file
	 * @param filename
	 */
	public CharacterData(File filename) {
		load(filename);
		
		if(fullName != null) {
			String fname = fullName.replaceAll(" ", "_").trim();
		
			backpack = new Inventory(fname);
			spellbook = new Spellbook(fname);
		}
	}
	
	/**
	 * Prints all info of the character plus size of backpack and known spells
	 */
	public String toString() {
		return ""
				+ "**Full Name**: " + fullName + "\n"
				+ "**Nickname**: " + nickname + "\n"
				+ "**Gender**: " + gender + "\n"
				+ "**Race**: " + race + "\n"
				+ "**Age**: " + age + "\n"
				+ "**Max MP**: " + mp + "\n"
				+ "**Unique Items in Inventory**: " + backpack.countUniqueItems() + "\n"
				+ "**Spells Known**:" + spellbook.spellCount() + "\n";
	}
	
	/**
	 * Refreshes the CharacterData, reads in everything again
	 */
	public void refresh() {
		String fileName = fullName.replaceAll(" ", "_").trim() + "_cd.txt";
		File file = new File(BotInfo.DBpath + BotInfo.DB_characters + "/" + fileName);
		load(file);
	}
	
	/**
	 * Adds specified amount of item to inventory
	 * @param item
	 * @param amount
	 */
	public void addToInventory(Item item, int amount) {
		backpack.add(item, amount);
	}
	
	/**
	 * Removes specified amount of item from inventory
	 * @param item
	 * @param amount
	 */
	public void removeFromInventory(Item item, int amount) {
		backpack.remove(item, amount);
	}
	
	/**
	 * Checks if passed in item is in inventory
	 * @param item
	 * @return
	 */
	public int getItemInInventory(Item item) {
		return backpack.get(item);
	}
	
	/**
	 * Returns entire category in inventory 
	 * @param category
	 * @return
	 */
	public String getCategory(String category) {
		return backpack.print(category);
	}
	
	/**
	 * Returns a hashmap of all items that contains passed in string
	 * @param string
	 * @return
	 */
	public HashMap<Item, Integer> search(String str) {
		return backpack.search(str);
	}
	
	/**
	 * Clears character's inventory
	 */
	public void clearInventory() {
		backpack.clearInventory();
	}
	
	/**
	 * Adds passed in spell to spellbook
	 * @param s
	 */
	public void addSpell(Spell s) {
		spellbook.addSpell(s);
	}
	
	/**
	 * Removes spell by passed in string
	 * @param s
	 */
	public void removeSpell(String s) {
		spellbook.removeSpell(s);
	}
	
	/**
	 * Finds spell by passed in name and returns
	 * @param s
	 * @return
	 */
	public Spell findSpell(String s) {
		return spellbook.findSpell(s);
	}
	
	/**
	 * Cast spell and reduces MP by spell cost
	 * @param s
	 * @return
	 */
	public boolean cast(String s) {
		Spell sp = findSpell(s);
		if(sp != null) {
			if(mp - sp.getCost() < 0)
				return false;
			else {
				mp -= sp.getCost();
				save();
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Restores to mp to mpCap
	 */
	public void restoreMP() {
		mp = mpCap;
		save();
	}
	
	/**
	 * Saves the character data into the DB file
	 */
	private void save() {
		
		String fileName = fullName.replaceAll(" ", "_").trim() + "_cd.txt";
		File file = new File(BotInfo.DBpath + BotInfo.DB_characters + "/" + fileName);
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			
			writer.write(fullName);
			writer.newLine();
			writer.write(firstName);
			writer.newLine();
			writer.write(nickname);
			writer.newLine();
			writer.write(gender);
			writer.newLine();
			writer.write(race);
			writer.newLine();
			writer.write(age);
			writer.newLine();
			writer.write(mp+"");
			writer.newLine();
			writer.write(mpCap+"");
			writer.newLine();
			
			writer.close();
		} catch(Exception e) {
			System.out.println(e);
		}
	}
	
	/**
	 * Loads in character
	 */
	private void load(File file) {
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String args = reader.readLine();
			
			if(args != null || !args.equals("")) {
				this.fullName = args;
				this.firstName = reader.readLine();
				this.nickname = reader.readLine();
				this.gender = reader.readLine();
				this.race = reader.readLine();
				this.age = reader.readLine();
				this.mp = Integer.parseInt(reader.readLine());
				this.mpCap = Integer.parseInt(reader.readLine());
			}
			
			reader.close();
		} catch(Exception e) {
			System.out.println(e);
		}
	}
	
	/**
	 * Exclusively used for search command
	 * @return
	 */
	public String allStrings() {
		return (fullName + nickname + gender + race + age + mp + mpCap).toLowerCase();
	}
	
	//**********SETTERS**********
	
	public void setName(String name) {
		this.fullName = fullName;
		firstName = fullName.split(" ")[0];
		save();
	}
	
	public void setNickname(String name) {
		nickname = name;
		save();
	}
	
	public void setRace(String race) {
		this.race = race;
		save();
	}
	
	public void setGender(String gender) {
		this.gender = gender;
		save();
	}
	
	public void setAge(String age) {
		this.age = age;
		save();
	}
	
	public void setMPmax(int mpCap) {
		this.mpCap = mpCap;
		save();
	}
	
	public void setMP(int mp) {
		if(mp > mpCap)
			this.mp = mpCap;
		else if(mp < 0)
			this.mp = 0;
		else
			this.mp = mp;
		save();
	}

	//***************************
	
	//**********GETTERS**********
	
	public Inventory getInventory() {
		return backpack;
	}
	
	public Spellbook getSpellbook() {
		return spellbook;
	}
	
	public String getName() {
		return firstName;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public String getNickname() {
		return nickname;
	}
	
	public String getRace() {
		return race;
	}
	
	public String getAge() {
		return age;
	}
	
	public int getMP() {
		return mp;
	}
	
	public int getMaxMP() {
		return mpCap;
	}
	//****************************
	
}
