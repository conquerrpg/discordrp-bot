/**
 * File: PlayerProfile.java
 * Author: Matt Wong
 * Editors: Matt Wong
 * Desc: Tracks, holds and uses data for each member of the server
 */
package Conquer.DiscordRP_Bot;

import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import java.io.*;
import java.util.HashMap;

public class PlayerProfile implements Entity{

	//basic information
	private String name;
	private String email;
	private String username;
	private String id;
	private String fileName;

	//player's current hero/character
	private CharacterData hero;

	//player's private narration channel
	private MessageChannel channel;


	/**
	 * Constructor
	 * Takes in a the name of the player, the email address and the user object
	 * @param name
	 * @param email
	 * @param user
	 */
	public PlayerProfile(String name, String email, User user) {
		this.name = name;
		this.email = email;

		username = user.getName();
		id = user.getDefaultAvatarId();
		//assigns a filename if one doesn't exist
		if(fileName == null || fileName.equals("")) 
			fileName = username + " - " + id + ".txt";
		
		save();
	}

	/**
	 * Constructor
	 * Takes in a file and extract data to make a PlayerProfile
	 * @param file
	 * @param chans
	 */
	public PlayerProfile(File file, HashMap<String, MessageChannel> chans) {
		try {
			// reads in all data in specified order
			BufferedReader br = new BufferedReader(new FileReader(file));
			name = br.readLine();
			email = br.readLine();
			username = br.readLine();
			id = br.readLine();
			fileName = file.getName();
			String heroStr = br.readLine();
			if(!heroStr.equals("null")) {
				hero = new CharacterData(heroStr);
			}
			
			String channelStr = br.readLine();
			if(!channelStr.equals("null")) {
				channel = App.channels.get(channelStr);
			}
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Saves all data into the player's textfile
	 */
	private void save() {

		/*
		 * Writes to file in this order
		 * 	- Name
		 * 	- Email
		 * 	- Username
		 * 	- ID
		 * 	- Character Full Name
		 * 	- Player's Private Channel Name
		 * 
		 */
		File file = new File(BotInfo.DBpath + BotInfo.DB_profiles + "/" + fileName);
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));

			writer.write(name);
			writer.newLine();
			writer.write(email);
			writer.newLine();
			writer.write(username);
			writer.newLine();
			writer.write(id);
			writer.newLine();
			if(hero != null) {
				writer.write(hero.getFullName());
			}
			else
				writer.write("null");

			writer.newLine();

			if(channel != null) {
				writer.write(channel.getName());
			}
			else
				writer.write("null");

			writer.newLine();

			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}				
	}
	
	// GETTERS AND SETTERS 
	//*****************************************************************
	
	public String getName() {
		return name;
	}
	public String getEmail() {
		return email;
	}
	public String getID() {
		return id;
	}
	public String getfileName() {
		return fileName;
	}
	public String getUsername() {
		return username;
	}
	public CharacterData getHero() {
		return hero;
	}
	public MessageChannel getChannel() {
		return channel;
	}
	public void setName(String s) {
		name = s;
		save();
	}
	public void setEmail(String s) {
		email = s;
		save();
	}
	public void setID(String s) {
		id = s;
		save();
	}
	public void setUsername(String s) {
		username = s;
		save();
	}
	public void setChannel(MessageChannel c) {
		channel = c;
		save();
	}
	public void setCharacter(CharacterData c) {
		hero = c;
		save();
	}

	//*****************************************************************
	
	
	/**
	 * Returns a string of all listed information
	 */
	public String toString() {
		String str = "";
		str += "**Name**: " + name + "\n";
		str += "**Email**: " + email + "\n";
		str += "**Username**: " + username + "\n";
		str += "**ID**: " + id + "\n";
		str += "**File Name**: " + fileName + ".txt\n";

		str += "**Hero**: ";
		if(hero == null)
			str += "Unassigned";
		else
			str += hero.getFullName();
		str += "\n";

		str += "**Channel**: ";
		if(channel == null)
			str += "Unassigned";
		else
			str += channel.getName();
		str += "\n";

		return str;

	}
	
	/**
	 * Exclusively used for search command
	 * @return
	 */
	public String allStrings() {
		String toRet = name + email + username + id + fileName;
		if(hero != null)
			toRet += hero.getName();
		if(channel != null)
			toRet += channel.getName();
		return toRet.toLowerCase();
	}
}
