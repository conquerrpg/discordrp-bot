/**
 * File: BotInfo.java
 * Author: Matt Wong
 * Editors: Matt Wong
 * Desc: Simply holds static information about the bot
 */
package Conquer.DiscordRP_Bot;

import java.awt.Color;

public class BotInfo {
	
	public static final String token = "NTE3NzkwODAzNDQ4Mjk5NTIx.DuHWJw.lPTn_KG2VPU8XRe7Po0yD1K9PKs";
	public static final String prefix = ">";
	
	public static Color systemColor = Color.BLACK;
	public static Color dbColor = Color.GRAY;
	public static Color errColor = Color.RED;
	public static Color warningColor = Color.YELLOW;
	public static Color generalColor = Color.GREEN;
	public static Color inventoryColor = Color.BLUE;
	public static Color spellbookColor = Color.MAGENTA;
	public static Color mpColor = Color.CYAN;
	
	public static String DBpath = "db";//"C:\\Users\\matth\\Google Drive\\Conquer\\Conquer Divine Judgement\\botDB";
	public static String DB_profiles = "/profiles"; //remember to change the '/' back to '\\' when running on windows
	public static String DB_characters = "/characters";
	public static String DB_inventories = "/inventories";
	public static String DB_spellbooks = "/spellbooks";
	
	public static String DB_items = "/items";
	public static String DB_spells = "/spells";
}