package Conquer.DiscordRP_Bot;

import java.io.*;

public class Spell implements Entity {
	
	private String name;
	private String description;
	private int cost;
	
	/**
	 * Constructor
	 * @param n
	 * @param d
	 * @param c
	 */
	public Spell(String n, String d, int c) {
		name = n;
		description = d;
		cost = c;
		save();
	}
	
	/**
	 * Constructor with a file
	 * @param file
	 */
	public Spell(File file) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			name = reader.readLine();
			description = reader.readLine();
			cost = Integer.parseInt(reader.readLine());
			
			reader.close();
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * saves spell to the DB
	 */
	private void save() {
		File file = new File(BotInfo.DBpath + BotInfo.DB_spells + "/" + "spell_" + name + ".txt");
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			writer.write(name);
			writer.newLine();
			writer.write(description);
			writer.newLine();
			writer.write(cost + "");
			
			writer.close();
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * Returns a String for name, description and cost
	 */
	public String toString() {
		return "**Name**: " + name + "\n"
				+ "**Description**: " + description + "\n"
				+ "**Cost**: " + cost;
	}
	
	/**
	 * Exclusively used for search command
	 * @return
	 */
	public String allStrings() {
		return (name + description + cost).toLowerCase();
	}
	
	public String getName() {
		return name;
	}
	
	public String getDesc() {
		return description;
	}
	
	public int getCost() {
		return cost;
	}
	
	public void setName(String n) {
		name = n;
		save();
	}
	
	public void setDesc(String d) {
		description = d;
		save();
	}
	
	public void setCost(int c) {
		cost = c;
		save();
	}
}
