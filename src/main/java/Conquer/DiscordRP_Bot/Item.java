/**
 * File: Item.java
 * Author: Matt Wong, Kyle Fleskes
 * Editors: Matt Wong, Kyle Fleskes
 * Desc: Represents a generic item that a player character might have.
 */
package Conquer.DiscordRP_Bot;
import java.util.*;
import java.io.*;

public class Item implements Entity{

	protected String name;
	protected String description; //a text description of item, may include other things like special abilities, etc.
	protected double weight;
	protected String type;
	
	//General Note: if something doesn't have one of these variables, just leave a blank string or -1.
	
	/**
	 * Constructor for new item
	 * @param n
	 * @param d
	 * @param w
	 */
	public Item(String n, String d, double w, String t) {
		name = n;
		description = d;
		weight = w;
		if(!t.equals("currency") && !t.equals("equipment") && !t.equals("consumable") && !t.equals("important"))
			type = "misc";
		else
			type = t;
		save();
	}
	
	/**
	 * Constructor for item in DB
	 * @param file
	 */
	public Item(File file) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			name = reader.readLine();
			weight = Double.parseDouble(reader.readLine());
			description = reader.readLine();
			type = reader.readLine();
			
			reader.close();
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * Saves file of item in DB
	 */
	private void save() {
		if(type == null || type.equals(""))
			type = "misc";
		File file = new File(BotInfo.DBpath + BotInfo.DB_items + "/" + type + "-" + name + ".txt");
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			writer.write(name);
			writer.newLine();
			writer.write(weight + "");
			writer.newLine();
			writer.write(description);
			writer.newLine();
			writer.write(type);
			
			writer.close();
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public String toString() {
		String toReturn = ""
				+ "**Name**: " + name + "\n"
				+ "**Type**: " + type + "\n"
				+ "**Description**: " + description + "\n"
				+ "**Weight**: " + weight;
		return toReturn;
	}
	
	/**
	 * Exclusively used for search command
	 * @return
	 */
	public String allStrings() {
		return (name + description + weight + type).toLowerCase();
	}

	public String getName() {
		return name;
	}
	
	
	public String getDescription() {
		return description;
	}
	
	public double getWeight() {
		return weight;
	}
	
	public String getType() {
		return type;
	}
	
	public void setName(String n) {
		name = n;
		save();
	}
	
	public void setDescription(String d) {
		description = d;
		save();
	}
	
	public void setWeight(double w) {
		weight = w;
		save();
	}
	
	public void setType(String t) {
		type = t;
		save();
	}
	
}
