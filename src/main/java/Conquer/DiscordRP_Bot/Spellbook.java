/**
 * File: Spellbook.java
 * Author: Matt Wong
 * Editors: Matt Wong
 * Desc: Tracks a character's list of spells that the character knows how to cast
 */
package Conquer.DiscordRP_Bot;
import java.util.*;
import java.io.*;

public class Spellbook {

	private String characterName;
	private HashMap<String, Spell> spells;

	private String fileName;

	/**
	 * Constructor requires name of the character
	 * @param name
	 */
	public Spellbook(String name) {
		characterName = name;
		spells = new HashMap<String, Spell>();

		fileName = name + "_spellbook.txt";
		
		File file = new File(BotInfo.DBpath + BotInfo.DB_spellbooks + "/" + fileName);
		if(file.exists())
			read();
		else
			save();
	}
	
	public HashMap<String, Spell> getSpellbook(){
		return spells;
	}
	
	/**
	 * Takes in spell and check against DB. Adds in DB if not in
	 * @param spell
	 * @return
	 */
	public void addSpell(Spell spell) {
		if(!App.spells.containsKey(spell.getName())) {
			App.spells.put(spell.getName(), spell);
		}
		spells.put(spell.getName(), spell);
		save();
	}
	
	/**
	 * Takes in string and removes a spell by that name
	 * @param spellName
	 * @return
	 */
	public Spell removeSpell(String spellName) {
		Spell toRet = null;
		if(spells.containsKey(spellName)) {
			toRet = spells.get(spellName);
			spells.remove(spellName);
			save();
		}
		return toRet;
	}
	
	/**
	 * Finds passed in spell name and returns spell
	 * @param spellName
	 * @return
	 */
	public Spell findSpell(String spellName) {
		if(spells.containsKey(spellName))
			return spells.get(spellName);
		return null;
	}
	
	/**
	 * Returns a list of all spells and its details
	 */
	public String toString() {
		String toRet = characterName + "'s Spellbook\n"
				+ "Spell Count: " + spellCount() + "\n\n";
		
		int cnt = 1;
		for(String i : spells.keySet()) {
			toRet += cnt + ". " + spells.get(i).toString() + "\n";
			cnt++;
		}
		return toRet;
	}
	
	/**
	 * Returns the size of the spellbook
	 * @return
	 */
	public int spellCount() {
		return spells.size();
	}
	
	/**
	 * Performs read() again
	 */
	public void refresh() {
		read();
	}

	/**
	 * Reads the spellbook and fills out the hashmap
	 */
	private void read() {
		try {
			File file = new File(BotInfo.DBpath + BotInfo.DB_spellbooks + "/" + fileName);
			
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String arg1 = reader.readLine(); 
			if(arg1 == null || arg1.equals(""))
				return;
			
			arg1 = reader.readLine();
			
			//check read in spells against the database
			while(arg1 != null && !arg1.equals("")) {
				if(App.spells.containsKey(arg1)) {
					Spell toAdd = App.spells.get(arg1);
					spells.put(toAdd.getName(), toAdd);
					
				}
				arg1 = reader.readLine();
			}	
			reader.close();
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}

	}

	/**
	 * Saves the hashmap into the file
	 */
	private void save() {
		File file = new File(BotInfo.DBpath + BotInfo.DB_spellbooks + "/" + fileName);
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			//first thing to write is the character name
			writer.write(characterName);
			writer.newLine();

			for(String i : spells.keySet()) {
				writer.write(i);
				writer.newLine();
			}

			writer.close();
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
